package com.example.emailsender.controller;

import com.example.emailsender.resource.EmailMessage;
import com.example.emailsender.service.EmailSenderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class EmailController {
    private final EmailSenderService emailSenderService;

    public EmailController(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    @PostMapping("/api/v1/sender-email")
    public ResponseEntity  sendEmail(@RequestBody EmailMessage emailMessage){
        this.emailSenderService.sendEmail(emailMessage.getToEmail(),emailMessage.getSenderName(),emailMessage.getSubject(),emailMessage.getBody());
        return ResponseEntity.ok().body("Success");
    }
}
