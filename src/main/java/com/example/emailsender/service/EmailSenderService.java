package com.example.emailsender.service;

public interface EmailSenderService {
    String sendEmail(String toEmail, String senderName , String subject, String body );
}
