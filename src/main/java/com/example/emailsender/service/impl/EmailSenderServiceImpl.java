package com.example.emailsender.service.impl;
import com.example.emailsender.service.EmailSenderService;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.mail.javamail.JavaMailSender;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {
    @Autowired
    private  JavaMailSender mailSender;

    @Override
    public String sendEmail(String toEmail, String senderName, String subject, String body) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper= new MimeMessageHelper(mimeMessage);
            mimeMessageHelper.setSubject( subject);
            String html = "<!doctype html>\n" +
                    "<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"\n" +
                    "      xmlns:th=\"http://www.thymeleaf.org\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"viewport\"\n" +
                    "          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
                    "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
                    "    <title>Email</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div> <h1>" + subject + "</h1></div>\n" +
                    "\n" +
                    "<div> <p>" + body+ "</p></div>\n" +
                    "\n" +
                    "<div> <p>Yours Truly</p></div>\n" +
                    "\n" +
                    "<div> " + senderName + "</div>\n" +
                    "</body>\n" +
                    "</html>\n";
            // Sending the mail
            mimeMessageHelper.setText(html, true);
            mimeMessageHelper.setTo(toEmail);
            mailSender.send(mimeMessage);
            return "Mail Sent Successfully...";
        }catch (Exception e){
            e.printStackTrace();
        }


        return toEmail;
    }
}
